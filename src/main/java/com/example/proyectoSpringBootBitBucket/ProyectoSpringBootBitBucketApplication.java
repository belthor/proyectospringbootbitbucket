package com.example.proyectoSpringBootBitBucket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * The Class ProyectoSpringBootBitBucketApplication.
 */
@SpringBootApplication
public class ProyectoSpringBootBitBucketApplication {

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
	public static void main(String[] args) {
		SpringApplication.run(ProyectoSpringBootBitBucketApplication.class, args);

	}

}
