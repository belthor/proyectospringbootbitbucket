package com.example.proyectoSpringBootBitBucket.constants;

public class CodeErrorConstants {

  public static final String INTERNAL_SERVER_ERROR = "500";
}
