package com.example.proyectoSpringBootBitBucket.controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.proyectoSpringBootBitBucket.constants.CodeErrorConstants;
import com.example.proyectoSpringBootBitBucket.entities.Category;
import com.example.proyectoSpringBootBitBucket.interfaces.CategoryServiceInterface;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;

/**
 * The Class CategoriaController.
 */
@RestController
@RequestMapping("/CategoriaController")
public class CategoryController {

	@Autowired
	private CategoryServiceInterface categoryService;

	@Operation(summary = "Get categorie filtered by Name", responses = {
			@ApiResponse(responseCode = CodeErrorConstants.INTERNAL_SERVER_ERROR, description = "Internal server error ,data access fails, you may have docker turned off"),
			@ApiResponse(responseCode = "200", description = "OK") })
	@GetMapping("${endpoints.categoriaController.getCategoriaByNameMethod}/{name}")
	public ResponseEntity<Optional<Category>> getCategoriaByName(@PathVariable final String name) {

		Optional<Category> c = categoryService.getCategoryByName(name);
		if (c.isPresent()) {
			return new ResponseEntity<Optional<Category>>(c, HttpStatus.OK);
		}

		return new ResponseEntity<Optional<Category>>(c, HttpStatus.NOT_FOUND);
	}

	@Operation(summary = "Get categorie filtered by Id", responses = {
			@ApiResponse(responseCode = CodeErrorConstants.INTERNAL_SERVER_ERROR, description = "Internal server error ,data access fails, you may have docker turned off"),
			@ApiResponse(responseCode = "200", description = "OK") })
	@GetMapping("${endpoints.categoriaController.getCategoriaByIdMethod}/{id}")
	public ResponseEntity<Optional<Category>> getCategoriaById(@PathVariable final Integer id) {

		Optional<Category> c = categoryService.getCategoryById(id);
		if (c.isPresent()) {
			// si encuentra devuelve objeto y codigo 200
			return new ResponseEntity<Optional<Category>>(c, HttpStatus.OK);
		}
		// si no encentra la categoria devuelve el 404
		return new ResponseEntity<Optional<Category>>(c, HttpStatus.NOT_FOUND);
	}

	@Operation(summary = "Get all categories from docker database", responses = {
			@ApiResponse(responseCode = CodeErrorConstants.INTERNAL_SERVER_ERROR, description = "Internal server error ,data access fails, you may have docker turned off"),
			@ApiResponse(responseCode = "200", description = "OK") })
	@GetMapping("${endpoints.categoriaController.getAllCategoriesMethod}")
	public ResponseEntity<Optional<List<Category>>> getAllCategories() {

		Optional<List<Category>> lista = Optional.empty();
		if (lista.isPresent()) {
			return new ResponseEntity<Optional<List<Category>>>(lista, HttpStatus.OK);

		}
		return new ResponseEntity<Optional<List<Category>>>(lista, HttpStatus.NOT_FOUND);
	}

	@Operation(summary = "Get categorie filtered by Name and id", responses = {
			@ApiResponse(responseCode = CodeErrorConstants.INTERNAL_SERVER_ERROR, description = "Internal server error ,data access fails, you may have docker turned off"),
			@ApiResponse(responseCode = "200", description = "OK") })
	@GetMapping("${endpoints.categoriaController.getCategoriaByNameAndIdMethod}")
	public ResponseEntity<Optional<Category>> getCategoriaByNameAndId(@RequestParam final String name, @RequestParam final Integer id) {

		Optional<Category> c = categoryService.getCategoryByNameAndId(name, id);

		if (c.isPresent()) {
			return new ResponseEntity<Optional<Category>>(c, HttpStatus.OK);
		}
		return new ResponseEntity<Optional<Category>>(c, HttpStatus.NOT_FOUND);
	}

}
