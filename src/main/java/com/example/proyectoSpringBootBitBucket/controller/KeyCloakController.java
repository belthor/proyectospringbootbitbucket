package com.example.proyectoSpringBootBitBucket.controller;

import java.security.interfaces.RSAPublicKey;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.auth0.jwk.Jwk;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.proyectoBootBitBucket.exceptions.BussinesRuleException;
import com.example.proyectoSpringBootBitBucket.interfaces.KeycloakRestServiceInterface;
import com.example.proyectoSpringBootBitBucket.services.JwtServiceImpl;

@RestController
@RequestMapping("/KeyCloakController")
public class KeyCloakController {
	private Logger logger = LoggerFactory.getLogger(KeyCloakController.class);

	@Autowired
	private KeycloakRestServiceInterface restService;

	@Autowired
	private JwtServiceImpl jwtService;

	@GetMapping("${endpoints.KeyCloakController.getRolesMethod}")
	public ResponseEntity<?> getRoles(@RequestHeader("Authorization") String authHeader) throws BussinesRuleException {
		try {
			DecodedJWT jwt = JWT.decode(authHeader.replace("Bearer", "").trim());

			// check JWT is valid
			Jwk jwk = jwtService.getJwk();
			Algorithm algorithm = Algorithm.RSA256((RSAPublicKey) jwk.getPublicKey(), null);

			algorithm.verify(jwt);

			// check JWT role is correct

			List<String> roles = ((List) jwt.getClaim("realm_access").asMap().get("roles"));

			// check JWT is still active
			Date expiryDate = jwt.getExpiresAt();
			if (expiryDate.before(new Date())) {
				throw new Exception("token is expired");
			}
			// all validation passed
			HashMap<String, Integer> HashMap = new HashMap<String, Integer>();
			for (String str : roles) {
				HashMap.put(str, str.length());
			}
			return ResponseEntity.ok(HashMap);
		} catch (Exception e) {
			logger.error("exception : {} ", e.getMessage());
			throw new BussinesRuleException("01", e.getMessage(), HttpStatus.FORBIDDEN);
		}
	}

	@GetMapping("${endpoints.KeyCloakController.validMethod}")
	public ResponseEntity<?> valid(@RequestHeader("Authorization") String authHeader) throws BussinesRuleException {
		try {
			restService.checkValidity(authHeader);
			return ResponseEntity.ok(new HashMap() {
				{
					put("is_valid", "true");
				}
			});
		} catch (Exception e) {
			logger.error(e.getMessage());
			throw new BussinesRuleException("is_valid", "False", HttpStatus.FORBIDDEN);

		}
	}

	@PostMapping(value = "${endpoints.KeyCloakController.loginMethod}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> login(@RequestParam String username, @RequestParam String password) {
		String login = restService.login(username, password);
		return ResponseEntity.ok(login);
	}

	@PostMapping(value = "/logout", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> logout(@RequestParam(value = "refresh_token", name = "refresh_token") String refreshToken)
			throws BussinesRuleException {
		try {
			restService.logout(refreshToken);
			return ResponseEntity.ok(new HashMap() {
				{
					put("logout", "true");
				}
			});
		} catch (Exception e) {
			logger.error("unable to logout, exception : {} ", e.getMessage());
			throw new BussinesRuleException("logout", "False", HttpStatus.FORBIDDEN);
		}
	}

	@PostMapping(value = "${endpoints.KeyCloakController.refreshMethod}", produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<?> refresh(@RequestParam(value = "refresh_token", name = "refresh_token") String refreshToken)
			throws BussinesRuleException {
		try {
			return ResponseEntity.ok(restService.refresh(refreshToken));
		} catch (Exception e) {
			logger.error("unable to refresh, exception : {} ", e.getMessage());
			throw new BussinesRuleException("refresh", "False", HttpStatus.FORBIDDEN);
		}
	}

}
