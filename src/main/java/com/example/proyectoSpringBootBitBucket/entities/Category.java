package com.example.proyectoSpringBootBitBucket.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

/**
 * The Class Category.
 */
@Entity
@Table(name = "Categorias")
@Data
public class Category {

	/** The id. */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // suma 1 la ultima clave de la base de datos
	@Column(name = "Id", nullable = false)
	private int id;

	/** The nombre. */
	@Column(name = "Nombre", nullable = false)
	private String name;

	/** The description. */
	@Column(name = "descripcion", nullable = false)
	private String description;

}

//
// linia 24,29 y 34 nun faen falta si el nombre se corresponde con el campo de la tabla de la base
// de datos
