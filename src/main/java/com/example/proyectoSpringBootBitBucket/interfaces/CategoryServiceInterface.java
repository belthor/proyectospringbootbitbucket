package com.example.proyectoSpringBootBitBucket.interfaces;

import java.util.List;
import java.util.Optional;

import com.example.proyectoSpringBootBitBucket.entities.Category;

/**
 * The Interface CategoriaServiceInterface.
 */
public interface CategoryServiceInterface {

	public Optional<Category> getCategoryByName(String nombre);

	public Optional<Category> getCategoryById(Integer id);

	public Optional<List<Category>> getAllCategories();

	public Optional<Category> getCategoryByNameAndId(String name, Integer id);

}
