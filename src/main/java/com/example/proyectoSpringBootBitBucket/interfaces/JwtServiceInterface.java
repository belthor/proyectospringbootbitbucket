package com.example.proyectoSpringBootBitBucket.interfaces;

import com.auth0.jwk.Jwk;

public interface JwtServiceInterface {

	public Jwk getJwk() throws Exception;

}
