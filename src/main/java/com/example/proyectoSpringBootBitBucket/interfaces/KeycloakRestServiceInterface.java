package com.example.proyectoSpringBootBitBucket.interfaces;

import java.util.List;

public interface KeycloakRestServiceInterface {

	public String login(String username, String password);

	public String checkValidity(String token) throws Exception;

	public void logout(String refreshToken) throws Exception;

	public List<String> getRoles(String token) throws Exception;

	public String refresh(String refreshToken) throws Exception;

}
