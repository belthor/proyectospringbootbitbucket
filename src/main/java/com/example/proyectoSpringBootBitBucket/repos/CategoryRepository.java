package com.example.proyectoSpringBootBitBucket.repos;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.proyectoSpringBootBitBucket.entities.Category;

/**
 * The Interface CategoriaRepository.
 */
@Repository
public interface CategoryRepository extends JpaRepository<Category, Integer> {

	@Query(value = "select * from Categorias where nombre=?1", nativeQuery = true)
	public Optional<Category> findCategoriaByName(String name);

	@Query(value = "select * from Categorias where nombre=?1 and Id=?2", nativeQuery = true)
	public Optional<Category> findCategoriaByNameAndId(String name, Integer id);

}
