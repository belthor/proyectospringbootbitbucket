package com.example.proyectoSpringBootBitBucket.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.proyectoSpringBootBitBucket.entities.Category;
import com.example.proyectoSpringBootBitBucket.interfaces.CategoryServiceInterface;
import com.example.proyectoSpringBootBitBucket.repos.CategoryRepository;

/**
 * The Class Category ServiceImpl.
 */
@Service
public class CategoryServiceImpl implements CategoryServiceInterface {

	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public Optional<Category> getCategoryByName(String name) {
		return categoryRepository.findCategoriaByName(name);
	}

	@Override
	public Optional<Category> getCategoryById(Integer id) {
		return categoryRepository.findById(id);
	}

	@Override
	public Optional<List<Category>> getAllCategories() {
		return Optional.ofNullable(categoryRepository.findAll());
	}

	@Override
	public Optional<Category> getCategoryByNameAndId(String name, Integer id) {
		return categoryRepository.findCategoriaByNameAndId(name, id);

	}

}
