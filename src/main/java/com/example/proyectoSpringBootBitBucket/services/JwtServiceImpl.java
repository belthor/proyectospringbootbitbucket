package com.example.proyectoSpringBootBitBucket.services;

import java.net.URL;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.auth0.jwk.Jwk;
import com.auth0.jwk.UrlJwkProvider;
import com.example.proyectoSpringBootBitBucket.interfaces.JwtServiceInterface;

@Service
public class JwtServiceImpl implements JwtServiceInterface {

	@Value("${keycloak.jwk-set-uri}")
	private String jwksUrl;

	@Value("${keycloak.certs-id}")
	private String certsId;

	@Override
	public Jwk getJwk() throws Exception {
		URL url = new URL(jwksUrl);
		UrlJwkProvider urlJwkProvider = new UrlJwkProvider(url);
		Jwk get = urlJwkProvider.get(certsId.trim());
		return get;
	}

}
