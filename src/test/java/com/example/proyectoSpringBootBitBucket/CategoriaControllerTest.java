package com.example.proyectoSpringBootBitBucket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;

import com.example.proyectoSpringBootBitBucket.controller.CategoryController;
import com.example.proyectoSpringBootBitBucket.interfaces.CategoryServiceInterface;

/**
 * The Class CategoriaControllerTest.
 */
@WebMvcTest(CategoryController.class)
public class CategoriaControllerTest {

	@Autowired
	private CategoryServiceInterface categoryService;

}
