package com.example.proyectoSpringBootBitBucket.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.test.web.servlet.MockMvc;

import com.example.proyectoSpringBootBitBucket.entities.Category;
import com.example.proyectoSpringBootBitBucket.interfaces.CategoryServiceInterface;

public class CategoriaControllerTest {

	@Autowired
	private MockMvc mockMVC;

	@Mock
	private CategoryServiceInterface categoryServiceInterfaceMock;

	// CLASS TO TEST
	@InjectMocks
	private CategoryController categoryControllerMock;

	@BeforeEach
	public void beforeTest() {
		MockitoAnnotations.openMocks(this);
	}

	@Test
	public void getCategoriaByNameTest_OK() {

		// Category categoryMock = Mockito.mock(Category.class);
		Category categoryMock = new Category();
		categoryMock.setId(1);
		categoryMock.setName("categoriaPrueba");
		categoryMock.setDescription("esto es una descripcion mockeada");

		// MOCKEAMOS LA LLAMADA AL METODO DEL SERVICIO
		Optional<Category> categoryOptionalMock = Optional.of(categoryMock);
		when(categoryServiceInterfaceMock.getCategoryById(Mockito.anyInt())).thenReturn(categoryOptionalMock);

		// LLAMAMOS A TRAVES DEL CONTROLLER QUE A SU VEZ LLAMA AL SERVICIO
		ResponseEntity<Optional<Category>> response = this.categoryControllerMock.getCategoriaById(2);

		// VERIFICAMOS LA LLAMADA Y HACEMOS ASSERT
		verify(this.categoryServiceInterfaceMock, times(1)).getCategoryById(2);
		assertEquals(response.getBody().get().getName(), "categoriaPrueba");

		System.out.println(response.getStatusCode());
		System.out.println(response);
		assertEquals(response.getStatusCode(), 200);

	}

	@Test
	public void getCategoriaByNameTest_KO() {

		// Category categoryMock = Mockito.mock(Category.class);
		Category categoryMock = null;

		// MOCKEAMOS LA LLAMADA AL METODO DEL SERVICIO
		Optional<Category> categoryOptionalMock = Optional.empty();
		when(categoryServiceInterfaceMock.getCategoryById(Mockito.anyInt())).thenReturn(categoryOptionalMock);

		// LLAMAMOS A TRAVES DEL CONTROLLER QUE A SU VEZ LLAMA AL SERVICIO
		ResponseEntity<Optional<Category>> response = this.categoryControllerMock.getCategoriaById(2);

		System.out.println(response.getStatusCode());
		System.out.println(response);
		// VERIFICAMOS LA LLAMADA Y HACEMOS ASSERT
		verify(this.categoryServiceInterfaceMock, times(1)).getCategoryById(2);
		assertEquals(response.getStatusCodeValue(), 404);

	}

}
